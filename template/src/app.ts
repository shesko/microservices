import "reflect-metadata"
import {createExpressServer} from "routing-controllers"
import {GreetingController} from "./controllers/greeter"
import {NextFunction, Request, Response} from 'express'


const routingControllerOptions = createExpressServer({
    controllers: [GreetingController]
});

const app = createExpressServer(routingControllerOptions);

const logRequestStart = (req: Request, res: Response, next: NextFunction) => {

    res.on('finish', () => {
        console.info(`${req.method} ${req.originalUrl}\n  --> ${res.statusCode} ${res.statusMessage}`)
    })

    next()
}

app.use(logRequestStart)

app.listen(3000)

console.log("Express server is running on port 3000.")
