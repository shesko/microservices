import "reflect-metadata"
import {Get, JsonController} from "routing-controllers"


@JsonController()
export class GreetingController {

    @Get("/hello")
    getGreeting() {
        return {greeting: "Hello!"}
    }
}
