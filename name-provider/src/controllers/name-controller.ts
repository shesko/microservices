import "reflect-metadata"
import {Get, JsonController} from "routing-controllers"

@JsonController()
export class NameController {

    @Get("/name")
    getName() {
        const names = [
            "Francesco",
            "Sonya",
            "Jimmy",
            "Antonio",
            "Sebastian",
            "Akey",
            "Mohammed",
            "Sarah",
            "Victor"
        ]
        return { name: names[Math.floor(Math.random() * names.length)] }
    }
}
